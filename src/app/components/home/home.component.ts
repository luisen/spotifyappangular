import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [],
})
export class HomeComponent {
  newReleases: any = [];
  loading: boolean;
  error = false;
  message: string;

  constructor(private spotify: SpotifyService) {
    this.loading = true;
    this.error = false;

    this.spotify.getNewReleases().subscribe(
      (data: any) => {
        console.log(data);
        this.newReleases = data;
        this.loading = false;
      },
      (errorService) => {
        this.loading = false;
        this.error = true;
        console.log(errorService);
        console.log(errorService.error.error.message);
        this.message = errorService.error.error.message;
      }
    );
  }
}
